import AppController from "./App.controller";

function App() {
  return <AppController />;
}

export default App;

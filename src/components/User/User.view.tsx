import { UserProps } from "src/components/User/User.model";
import { MdLocationPin } from "react-icons/md";
import { Link } from "react-router-dom";
import "./User.css";

export default function UserView({
  login,
  avatar_url,
  followers,
  following,
  location,
}: UserProps) {
  return (
    <div className="user-component">
      <img src={avatar_url} alt="Foto do avatar" />
      <h2>{login}</h2>
      {location && (
        <p className="uc-location">
          <MdLocationPin />
          <span>{location}</span>
        </p>
      )}
      <div className="uc-stats">
        <div>
          <p>Seguidores:</p>
          <p className="uc-number">{followers}</p>
        </div>
        <div>
          <p>Seguindo:</p>
          <p className="uc-number">{following}</p>
        </div>
      </div>
      <Link to={`https://github.com/${login}`}>Ver melhores projetos</Link>
    </div>
  );
}

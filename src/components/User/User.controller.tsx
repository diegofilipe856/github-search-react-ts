import UserView from "./User.view";
import {UserProps} from "./User.model";

export default function UserController(props:UserProps) {
  return (<UserView {...props}/>);
}

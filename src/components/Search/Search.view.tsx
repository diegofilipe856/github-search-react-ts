import "./Search.css";
import { BsSearch } from "react-icons/bs";
import { SearchProps } from "./Search.model";

export default function SearchView({
  loadUser,
  userName,
  setUserName,
  handleKeyDown,
}: SearchProps) {
  return (
    <div className="search-component">
      <h2>Busque por um usuário:</h2>
      <p>Visualizar repositórios melhor avaliados</p>
      <div className="search-container">
        <input
          type="text"
          placeholder="Digite nick de usuário"
          onChange={(e) => {
            setUserName(e.target.value);
          }}
          onKeyDown={(e) => {handleKeyDown(e)}}
        />
        <button onClick={() => loadUser(userName)}>
          <BsSearch />
        </button>
      </div>
    </div>
  );
}

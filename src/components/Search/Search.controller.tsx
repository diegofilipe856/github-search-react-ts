import SearchView from "./Search.view";
import { useState } from "react";
import {Fase, LoadUserProp} from "./Search.model"

export default function SearchController({ loadUser }: LoadUserProp) {
  const [userName, setUserName] = useState<string>("");
  const handleKeyDown = (event: Fase) => {
    if (event.key === "Enter") {
      loadUser(userName);
    }
  };
  return (
    <SearchView
      loadUser={loadUser}
      userName={userName}
      setUserName={setUserName}
      handleKeyDown={handleKeyDown}
    />
  );
}

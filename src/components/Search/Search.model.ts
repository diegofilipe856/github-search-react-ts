export type SearchProps = {
  loadUser: (userName: string) => Promise<void>;
  userName: string;
  setUserName: (userName: string) => void;
  handleKeyDown: (event: Fase) => void;
};

export interface Fase {
  key: string;
}
export type LoadUserProp ={
  loadUser: (userName: string) => Promise<void>;
}
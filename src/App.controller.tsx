import AppView from './App.view';


const AppController: React.FC = () => {
  return <AppView />;
};

export default AppController;

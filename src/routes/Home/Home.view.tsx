import Search from "/src/components/Search";
import User from "/src/components/User";
import Error from "/src/components/Error";
import { HomeProps } from "./Home.model";
const HomeView = ({loadUser, user, error}:HomeProps) =>{
    return(
        <>
        <Search loadUser={loadUser}/>
        {user && <User {...user}/>}
        {error && <Error/>}
        </>
    )
}

export default HomeView;
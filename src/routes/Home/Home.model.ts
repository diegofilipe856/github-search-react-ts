import { UserProps } from "/src/components/User/User.model";

export interface HomeProps {
    loadUser: (userName: string) => Promise<void>;
    user: UserProps | null;
    error: boolean;
}
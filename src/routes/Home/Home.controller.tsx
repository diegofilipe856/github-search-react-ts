import HomeView from "./Home.view";
import { useState } from "react";
import { UserProps } from "src/components/User/User.model";

const HomeController = () => {
  const [user, setUser] = useState<UserProps | null>(null);
  const [error, setError] = useState(false);

  const loadUser = async(userName: string) =>{
    setError(false);
    setUser(null);
    const res = await fetch(`https://api.github.com/users/${userName}`)

    const data = await res.json()

    if (res.status === 404){
      setError(true)
      return;
    }

    const {avatar_url, login, location, followers, following} = data
    const userData: UserProps = {
      avatar_url,
            login,
            location,
            followers,
            following
    }
    setUser(userData)
  }
  return <HomeView loadUser={loadUser} user={user} error={error}/>;
};

export default HomeController;
